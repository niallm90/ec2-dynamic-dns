import json
import boto3
import botocore
import logging
import ipaddress
import math
import uuid
import random
import time
import re
import os
import sys
import copy


# iid = EC2 Instance ID
TXT_ID_RECORD_TEMPLATE = 'heritage=ec2-dynamic-dns,ec2-dynamic-dns/owner={iid}'


ec2 = boto3.resource('ec2')
route53 = boto3.client('route53')

logger = logging.getLogger('simple_example')
logger.setLevel(os.getenv('LOG_LEVEL', 'ERROR'))

# TTL to use for DNS records if the instance doesn't specify one
default_ttl = int(os.getenv('DEFAULT_TTL', 60))


class HostedZones():
    def __init__(self):
        self.hosted_zones = []
        self.load()

    def load(self):
        result = route53.list_hosted_zones()['HostedZones']

        for hz in result:
            self.hosted_zones.append(HostedZone.from_dict(hz))

    def get_hosted_zones(self, name):
        name = name + ('.' if name[-1] != '.' else '')
        for hosted_zone in filter(
                lambda x: x.name == name, self.hosted_zones):
            hosted_zone.load()
            yield hosted_zone

    def create_private_hosted_zone(self, name, vpc_region, vpc_id):
        result = route53.create_hosted_zone(
            Name=name,
            VPC={
                'VPCRegion': vpc_region,
                'VPCId': vpc_id
            },
            CallerReference=str(uuid.uuid1()),
            HostedZoneConfig={
                'Comment': 'Updated by ec2-dynamic-dns',
                'PrivateZone': True
            })

        hosted_zone = HostedZone(result['HostedZone']['Id'])
        hosted_zone.load()
        return hosted_zone


class HostedZone():
    def __init__(self, zone_id):
        self.zone_id = zone_id
        self._vpcs = None

    def load(self):
        data = route53.get_hosted_zone(Id=self.zone_id)
        self._load_dict(
            data['HostedZone'], data['VPCs'] if 'VPCs' in data else None)

    @classmethod
    def from_dict(cls, data):
        new = cls(data['Id'])
        new._load_dict(data)
        return new

    def _load_dict(self, data, vpcs=None):
        self.name = data['Name']
        self.private = data['Config']['PrivateZone']
        if self.private and vpcs:
            self._vpcs = [x['VPCId'] for x in vpcs]

    def __repr__(self):
        return '{}("{}")'.format(type(self).__name__, repr(self.zone_id))

    @property
    def vpcs(self):
        if not self.private:
            raise TypeError("Public zones don't have VPC associations")

        if self._vpcs is None:
            self.load()

        return self._vpcs

    def associate_vpc(self, vpc_region, vpc_id):
        if not self.private:
            raise ValueError("Cannot associate a VPC with a public zone")

        if vpc_id in self.vpcs:
            # Already associated
            return

        route53.associate_vpc_with_hosted_zone(
            HostedZoneId=self.zone_id,
            VPC={
                'VPCRegion': vpc_region,
                'VPCId': vpc_id
            },
            Comment='Updated by ec2-dynamic-dns')

    def record_sets(self):
        paginator = route53.get_paginator('list_resource_record_sets')

        source_zone_records = paginator.paginate(HostedZoneId=self.zone_id)
        for record_set in source_zone_records:
            for r in record_set['ResourceRecordSets']:
                yield RecordSet.from_dict(self.zone_id, r)

    def new_record_set(self, name, record_type, ttl, records):
        # If the name isn't fully qualified append the zone name
        if name[-1] != '.':
            name = '{}.{}'.format(name, self.name)

        return RecordSet(self.zone_id, name, record_type, ttl, records)


class RecordSet():
    def __init__(self, hosted_zone_id, name, record_type, ttl, records):
        self.hosted_zone_id = hosted_zone_id
        self.name = name
        self.type = record_type
        self.ttl = ttl
        self.records = records

    @classmethod
    def from_dict(cls, hosted_zone_id, data):
        records = {r['Value'].strip('"') for r in data['ResourceRecords']}
        return cls(
            hosted_zone_id,
            data['Name'],
            data['Type'],
            data['TTL'],
            records)

    def merge(self, other):
        self.records.update(other.records)

    def __repr__(self):
        return '{}({}, {}, {}, {}, {})'.format(
            type(self).__name__,
            repr(self.hosted_zone_id),
            repr(self.name),
            repr(self.type),
            repr(self.ttl),
            repr(self.records))

    def _update(self, action):
        assert action in ('create', 'update', 'delete')

        logger.info("{} {} record in {} for {} => {}".format(
            action.capitalize(),
            repr(self.type),
            repr(self.hosted_zone_id),
            repr(self.name),
            repr(self.records)))

        aws_action_map = {
            'create': 'CREATE',
            'update': 'UPSERT',
            'delete': 'DELETE'
        }

        if self.type == 'TXT':
            records = [{'Value': '"{}"'.format(r)} for r in self.records]
        else:
            records = [{'Value': r} for r in self.records]

        route53.change_resource_record_sets(
            HostedZoneId=self.hosted_zone_id,
            ChangeBatch={
                'Comment': 'Updated by ec2-dynamic-dns',
                'Changes': [{
                    'Action': aws_action_map[action],
                    'ResourceRecordSet': {
                        'Name': self.name,
                        'Type': self.type,
                        'TTL': self.ttl,
                        'ResourceRecords': records
                    }
                }]
            })

    def create(self):
        self._update('create')

    def update(self):
        self._update('update')

    def delete(self):
        self._update('delete')


class IPv4Network(ipaddress.IPv4Network):
    @property
    def reverse_pointer(self):
        octets = round(self.prefixlen / 8.0)
        segments = self.network_address.reverse_pointer.split('.')
        return '.'.join(segments[0 - octets - 2:]) + '.'


def check_vpc_configuration(vpc):
    dns_hostnames = vpc.describe_attribute(
        DryRun=False,
        Attribute='enableDnsHostnames')['EnableDnsHostnames']['Value']

    dns_support = vpc.describe_attribute(
        DryRun=False,
        Attribute='enableDnsSupport')['EnableDnsSupport']['Value']

    if not dns_hostnames:
        logger.error(
            "DNS hostnames disabled for VPC {}.".format(repr(vpc_id)))

    if not dns_support:
        logger.error(
            "DNS support disabled for VPC {}.".format(repr(vpc_id)))


class DomainMap():
    def __init__(self, domain, hostname=None):
        self.domain = domain
        self.hostname = hostname

    def is_reverse_lookup(self):
        return self.domain.endswith('.in-addr.arpa.')

    def __repr__(self):
        return '{}({}, {})'.format(
            type(self).__name__, repr(self.domain), repr(self.hostname))


def lambda_handler(event, context):
    instance_id = event['detail']['instance-id']

    region = event['region']

    try:
        instance = ec2.Instance(instance_id)
        instance.load()
    except Exception as e:
        logger.critical("Error getting instance: {}".format(e))
        return 500

    starting_states = (
        'pending',
        'running')
    stopping_states = (
        'shutting-down',
        'terminated',
        'stopping',
        'stopped')

    state = event['detail']['state']

    id_record = TXT_ID_RECORD_TEMPLATE.format(iid=instance.id)
    logger.info("Using ID record {} for TXT entries".format(repr(id_record)))

    if state in starting_states:
        return handle_ec2_start(instance, region, id_record)
    elif state in stopping_states:
        return handle_ec2_stop(instance, region, id_record)
    else:
        logger.critical("Unknown state {} for EC2 instance {}".format(
            repr(state), repr(instance.id)))
        return 500


def handle_ec2_stop(instance, region, id_record):
    logger.info("EC2 stop event for instance {}".format(repr(instance.id)))
    hosted_zones = HostedZones()

    for hz in hosted_zones.hosted_zones:
        record_sets = list(hz.record_sets())

        # Find all the records that match out identifier
        for id_record_set in filter(
                lambda x: x.type == 'TXT' and id_record in x.records,
                record_sets):
            logger.info("Found {} matching the identifier {}".format(
                repr(id_record_set), repr(id_record)))
            # Delete all the records with the same name that arn't TXT records
            for r in filter(
                    lambda x: x.type != 'TXT' and x.name == id_record_set.name,
                    record_sets):
                r.delete()

            # Clean up the text record last
            id_record_set.delete()

    return 200


def get_domain_maps(instance, tags):
    logger.info("EC2 start event for instance {}".format(repr(instance.id)))

    domain_maps = []

    logger.debug("Adding DHCP domain map from {}".format(
        repr(instance.vpc.dhcp_options.dhcp_configurations)))
    domain_maps.extend([
        DomainMap(x['Values'][0]['Value']) for x in
        instance.vpc.dhcp_options.dhcp_configurations
        if x['Key'] == 'domain-name'])

    logger.debug("Adding subnet domain map from {}".format(
        repr(instance.subnet)))
    network = IPv4Network(instance.subnet.cidr_block)
    domain_maps.append(DomainMap(network.reverse_pointer))

    try:
        hostname, domain = tags['dns'].split('.', 1)
        domain_maps.append(DomainMap(domain, hostname))
    except KeyError:
        logger.debug("No 'DNS' tag on instance {}.".format(
            repr(instance.id)))

    try:
        domain_maps.append(DomainMap(tags['zone']))
    except KeyError:
        logger.debug("No 'ZONE' tag on instance {}.".format(
            repr(instance.id)))

    return domain_maps


def handle_ec2_start(instance, region, id_record):
    return_code = 200

    check_vpc_configuration(instance.vpc)

    # Crude backoff timer
    time.sleep(random.random())

    if instance.tags:
        tags = {t['Key'].strip().lower(): t['Value'] for t in instance.tags}
    else:
        tags = {}
        logger.info("Instance {} has no tags.".format(repr(instance.id)))

    domain_maps = get_domain_maps(instance, tags)
    logger.info("Found domains to check for host {!s}".format(domain_maps))

    ttl = int(tags.get('ttl', default_ttl))

    hosted_zones = HostedZones()

    for domain_map in domain_maps:
        logger.info("Handling domain map: {!s}".format(domain_map))
        try:
            existing_hosted_zones = hosted_zones.get_hosted_zones(
                domain_map.domain)

            if (domain_map.is_reverse_lookup() and
                    len(list(existing_hosted_zones)) == 0):
                logger.info(
                    "Creating a new zone for reverse "
                    "lookup domain {}".format(repr(domain_map)))

                existing_hosted_zones.add(
                    hosted_zones.create_private_hosted_zone(
                        domain_map.domain, region, instance.vpc_id))

            for hosted_zone in existing_hosted_zones:
                logger.info(
                    "Found a {} hosted zone {} for domain map {}".format(
                        'private' if hosted_zone.private else 'public',
                        repr(hosted_zone.zone_id),
                        repr(domain_map)))

                target_ip = instance.private_ip_address \
                    if hosted_zone.private else \
                    instance.public_ip_address
                target_dns_name = instance.private_dns_name \
                    if hosted_zone.private else \
                    instance.public_dns_name

                if hosted_zone.private:
                    logger.info(
                        "Associating private hosted zone {} "
                        "with VPC {}".format(
                            repr(hosted_zone.zone_id),
                            repr(instance.vpc_id)))

                    hosted_zone.associate_vpc(region, instance.vpc_id)

                if not domain_map.is_reverse_lookup():
                    if domain_map.hostname:
                        record_set = hosted_zone.new_record_set(
                            domain_map.hostname,
                            'A',
                            ttl,
                            [target_ip])
                    else:
                        record_set = hosted_zone.new_record_set(
                            target_dns_name.split('.')[0],
                            'A',
                            ttl,
                            [target_ip])
                else:
                    reverse_ptr = ipaddress.ip_address(
                        instance.private_ip_address).reverse_pointer + '.'
                    record_set = hosted_zone.new_record_set(
                        reverse_ptr,
                        'PTR',
                        ttl,
                        [instance.private_dns_name])

                try:
                    logger.info("Creating {}".format(record_set))
                    record_set.create()

                    # Create the identifier record with the same name
                    id_record_set = copy.deepcopy(record_set)
                    id_record_set.type = 'TXT'
                    id_record_set.records = {id_record}
                    id_record_set.update()
                except Exception:
                    logger.info(
                        "Cannot create {} as record set already "
                        "exists in {}".format(
                            repr(record_set),
                            repr(hosted_zone)))

        except Exception:
            logger.exception(
                "Failed to perform action for domain map {}".format(
                    repr(domain_map)))
            return_code = 500

    return return_code


