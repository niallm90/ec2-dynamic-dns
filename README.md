# ec2-dynamic-dns

An AWS Lambda application to provide dynamic DNS for ec2 instance. Based on https://github.com/aws-samples/aws-lambda-ddns-function

This is mainly a rewrite of the project listed above to increase my understanding of the subject.

## Differences to aws-lambda-ddns-function

* Python v3
* A better seperation of concerns. An attempt was made to wrap most of the parsing into classes.
* Removes requirment of DynamoDB in favor or TXT records. Inspired by kops dns-controller. 
* Adds the ability to set the records TTL values though an EC2 Instance tag.
